//
//  MRGCoreDataStack.h
//  SimpleToDoList
//
//  Created by mac-249 on 12/1/15.
//  Copyright © 2015 mac-249. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

@interface MRGCoreDataStack : NSObject

@property (strong, nonatomic, readonly) NSManagedObjectContext *context;
@property (strong, nonatomic, readonly) NSPersistentStoreCoordinator *coordinator;

+ (instancetype)sharedStack;

- (void)saveContext;
- (void)removeObject:(NSManagedObject *)object;

@end
