//
//  NSDate+ConvertToString.m
//  SimpleToDoList
//
//  Created by mac-249 on 12/2/15.
//  Copyright © 2015 mac-249. All rights reserved.
//

#import "NSDate+ConvertToString.h"

@implementation NSDate (ConvertToString)

- (NSString *)dateToString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    NSString *stringDate = [dateFormatter stringFromDate:[NSDate date]];
    
    return stringDate;
}

@end
