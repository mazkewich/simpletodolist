//
//  NSDate+ConvertToString.h
//  SimpleToDoList
//
//  Created by mac-249 on 12/2/15.
//  Copyright © 2015 mac-249. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (ConvertToString)

- (NSString *)dateToString;

@end
