//
//  MRGCoreDataStack.m
//  SimpleToDoList
//
//  Created by mac-249 on 12/1/15.
//  Copyright © 2015 mac-249. All rights reserved.
//

#import "MRGCoreDataStack.h"

@interface MRGCoreDataStack ()

@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSPersistentStoreCoordinator *coordinator;
@property (strong, nonatomic) NSManagedObjectModel *model;

@end

@implementation MRGCoreDataStack

+ (instancetype)sharedStack
{
    static MRGCoreDataStack *stack = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        stack = [[MRGCoreDataStack alloc] init];
    });
    
    return stack;
}

- (NSManagedObjectContext *)context
{
    if (_context != nil)
    {
        return _context;
    }
    
    _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _context.persistentStoreCoordinator = self.coordinator;
    
    return _context;
}

- (NSManagedObjectModel *)model
{
    if (_model != nil)
    {
        return _model;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MRGSimpleToDoList" withExtension:@"momd"];
    _model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    NSAssert(_model != nil, @"Error creation model");
    
    return _model;
}

- (NSPersistentStoreCoordinator *)coordinator
{
    if (_coordinator != nil)
    {
        return _coordinator;
    }
    
    _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    
    NSURL *storeURL = [[self documentsDirectory] URLByAppendingPathComponent:@"MRGSimpleToDoList.sqlite"];
    NSError *error;
    NSPersistentStore *store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
    NSAssert(store != nil, @"Error initializing PSC: %@\n%@", [error localizedDescription], [error userInfo]);
    
    return _coordinator;
}

- (NSURL *)documentsDirectory
{
    NSURL *documentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    return documentsDirectory;
}

- (void)saveContext
{
    if ((_context != nil) && [_context hasChanges])
    {
        [_context save:nil];
    }
}

- (void)removeObject:(NSManagedObject *)object
{
    [_context deleteObject:object];
}

@end
