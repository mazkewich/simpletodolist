//
//  Note+CoreDataProperties.m
//  SimpleToDoList
//
//  Created by mac-249 on 12/1/15.
//  Copyright © 2015 mac-249. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Note+CoreDataProperties.h"

@implementation Note (CoreDataProperties)

@dynamic name;
@dynamic text;
@dynamic date;

@end
