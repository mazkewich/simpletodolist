//
//  MRGNoteViewController.h
//  SimpleToDoList
//
//  Created by mac-249 on 12/1/15.
//  Copyright © 2015 mac-249. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MRGNoteViewControllerDelegate;

@class Note;

@interface MRGNoteViewController : UIViewController

@property (weak, nonatomic) id <MRGNoteViewControllerDelegate> delegate;
@property (strong, nonatomic) Note *note;

@end

@protocol MRGNoteViewControllerDelegate <NSObject>

- (void)noteViewController:(MRGNoteViewController *)controller didSaveNote:(Note *)note;

@end