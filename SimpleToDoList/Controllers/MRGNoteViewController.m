//
//  MRGNoteViewController.m
//  SimpleToDoList
//
//  Created by mac-249 on 12/1/15.
//  Copyright © 2015 mac-249. All rights reserved.
//

#import "MRGNoteViewController.h"
#import "MRGCoreDataStack.h"
#import "Note.h"

@interface MRGNoteViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@end

@implementation MRGNoteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.nameField.text = self.note.name;
    self.descriptionTextView.text = self.note.text;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)cancelButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveButtonPressed:(id)sender
{
    NSManagedObjectContext *context = [MRGCoreDataStack sharedStack].context;
    Note *newNote = self.note;
    if (!newNote) {
        newNote = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:context];
        newNote.date = [NSDate date];
    }
    newNote.name = self.nameField.text;
    newNote.text = self.descriptionTextView.text;	
    
    [[MRGCoreDataStack sharedStack] saveContext];
    
    [self.delegate noteViewController:self didSaveNote:newNote];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
