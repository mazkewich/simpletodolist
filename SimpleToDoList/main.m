//
//  main.m
//  SimpleToDoList
//
//  Created by mac-249 on 12/1/15.
//  Copyright © 2015 mac-249. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
