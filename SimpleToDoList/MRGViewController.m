//
//  MRGViewController.m
//  SimpleToDoList
//
//  Created by mac-249 on 12/1/15.
//  Copyright © 2015 mac-249. All rights reserved.
//

#import "MRGViewController.h"
#import "MRGNoteViewController.h"
#import "Note.h"
#import "NSDate+ConvertToString.h"
#import "MRGCoreDataStack.h"

static NSString *const MRGCellIdentifier = @"MRGNoteCell";
static NSString *const MRGAddNewNoteSegueIdentifier = @"AddNewNote";
static NSString *const MRGEditNote = @"EditNote";

@interface MRGViewController () <UITableViewDelegate, UITableViewDataSource, MRGNoteViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableWithNotes;
@property (strong, nonatomic) NSMutableArray <Note *> *notes;

@end

@implementation MRGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Note"];	
    NSManagedObjectContext *context = [MRGCoreDataStack sharedStack].context;
    NSArray *results = [context executeFetchRequest:request error:nil];
    self.notes = [NSMutableArray arrayWithArray:results];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:MRGAddNewNoteSegueIdentifier])
    {
        ((MRGNoteViewController *)segue.destinationViewController).navigationItem.title = @"New Note";
        ((MRGNoteViewController *)segue.destinationViewController).delegate = self;
    }
    
    if ([segue.identifier isEqualToString:MRGEditNote])
    {
        ((MRGNoteViewController *)segue.destinationViewController).navigationItem.title = ((Note *)sender).name;
        ((MRGNoteViewController *)segue.destinationViewController).delegate = self;
        ((MRGNoteViewController *)segue.destinationViewController).note = sender;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MRGCellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MRGCellIdentifier];
    }
    Note *note = self.notes[indexPath.row];
    cell.textLabel.text = note.name;
    cell.detailTextLabel.text = [note.date dateToString];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Note *selectedNote = self.notes[indexPath.row];
    [self performSegueWithIdentifier:MRGEditNote sender:selectedNote];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [[MRGCoreDataStack sharedStack] removeObject:self.notes[indexPath.row]];
        [self.notes removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark - MRGNoteViewControllerDelegate

- (void)noteViewController:(MRGNoteViewController *)controller didSaveNote:(Note *)note
{
    if (![self.notes containsObject:note])
    {
        [self.notes addObject:note];
    }
    [self.tableWithNotes reloadData];
}

@end
